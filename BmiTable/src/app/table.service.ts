import { Injectable, OnInit } from '@angular/core';
import { Character } from './characters/models/character.model';
import { BehaviorSubject } from 'rxjs';

export interface Column {
  from: number;
  to: number;
  characters: Character[];
}

@Injectable({
  providedIn: 'root'
})
export class TableService implements OnInit {

  private tableColumns: BehaviorSubject<Column[]> = new BehaviorSubject([]);

  constructor() { }

  ngOnInit(): void {
    this.initTableColumns();
  }

  public getPeriodObservable() {
    return this.tableColumns.asObservable();
  }

  public calculateTable(characters: Character[]): void {
    this.initTableColumns();
    for (let character of characters) {
      const currentTable = this.tableColumns.value;

      const normalizedYear = parseInt(character.birth_year.replace('BBY', ''));

      if (isNaN(normalizedYear)) {
        currentTable[ (currentTable.length - 1) ].characters.push(character);
      }
      else {

        currentTable.find(period => normalizedYear >= period.from && (normalizedYear < period.to || !period.to)).characters.push(character);
      }
      this.tableColumns.next(currentTable);
    }

  }

  private initTableColumns(): void {
    this.tableColumns.next([
      { from: 0, to: 20, characters: [] },
      { from: 20, to: 40, characters: [] },
      { from: 40, to: 60, characters: [] },
      { from: 60, to: 80, characters: [] },
      { from: 70, to: 100, characters: [] },
      { from: 100, to: null, characters: [] },
      { from: null, to: null, characters: [] } ]);
  }
}