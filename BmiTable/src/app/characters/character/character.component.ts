import { Component, OnInit, Input } from '@angular/core';
import { Character } from '../models/character.model';
import { BmiService, Color } from '../../shared/services/bmi.service';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: [ './character.component.scss' ]
})
export class CharacterComponent implements OnInit {

  constructor(private readonly bmiService: BmiService) { }

  @Input() character: Character;
  color: Color;

  ngOnInit() {
    this.color = this.bmiService.getColor(this.getBmi());
  }

  getBmi = () => this.bmiService.getBMI(this.character.mass, this.character.height);
}
