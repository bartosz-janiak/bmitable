import { Character } from './character.model';

export interface ApiResponse {
    count: number;
    next: string | null;
    previous: string | null;
    results: Character[];
}
