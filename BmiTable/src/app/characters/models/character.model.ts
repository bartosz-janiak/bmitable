export interface Character {
    name: string;
    height: number;
    mass: number;
    birth_year: string;
}