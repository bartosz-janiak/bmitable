import { Injectable } from '@angular/core';
import { ApiResponse } from './models/api-response.model';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Character } from './models/character.model';

@Injectable({
  providedIn: 'root'
})
export class CharacterService {

  constructor(private http: HttpClient) { }

  private characters: BehaviorSubject<Character[]> = new BehaviorSubject([]);
  private nextPage: string;
  private prevPage: string;

  public getCharactersObservable(): Observable<Character[]> {
    return this.characters.asObservable();
  }

  public getCharactersFromApi(): void {
    this.http.get<ApiResponse>('https://swapi.co/api/people/').subscribe(response => this.handleReceivingResponse(response));
  }

  public getNextCharacters(): void {
    if (this.nextPage) {
      this.http.get<ApiResponse>(this.nextPage).subscribe(response => this.handleReceivingResponse(response));
    }
  }

  public getPrevCharacters(): void {
    if (this.prevPage) {
      this.http.get<ApiResponse>(this.prevPage).subscribe(response => this.handleReceivingResponse(response));
    }
  }

  private handleReceivingResponse(response: ApiResponse) {
    this.nextPage = response.next;
    this.prevPage = response.previous;

    if (response.results) {
      this.characters.next(response.results);
    }
  }

}
