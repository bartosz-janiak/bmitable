import { Component, OnInit, OnDestroy } from '@angular/core';
import { CharacterService } from './characters/character.service';
import { Subscribable, Subscription } from 'rxjs';
import { Character } from './characters/models/character.model';
import { TableService, Column } from './table.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})

export class AppComponent implements OnInit, OnDestroy {

  charactersSubscription: Subscription;
  characters: Character[];


  constructor(
    private readonly characterService: CharacterService,
    private readonly tableService: TableService
  ) {
    this.charactersSubscription = this.characterService.getCharactersObservable().subscribe(characters => this.handleLoad(characters));
  }

  ngOnInit(): void {
    this.characterService.getCharactersFromApi();
  }

  ngOnDestroy(): void {
    this.charactersSubscription.unsubscribe();
  }

  handleLoad(characters: Character[]) {
    if (characters.length) {
      this.tableService.calculateTable(characters);
    }
  }

  prepareColumnHeader(column: Column) {
    if (column.from >= 0 && column.to) {
      return `${column.from} - ${column.to}BBY`;
    }

    if (column.from && !column.to) {
      return `${column.from}BBY++`;
    }

    return 'undefined';
  }

}
