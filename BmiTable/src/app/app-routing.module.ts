import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CharactersModule } from './characters/characters.module';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    CharactersModule
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
