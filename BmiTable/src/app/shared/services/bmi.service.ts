import { Injectable } from '@angular/core';
import { isNumber } from 'util';

interface BmiScale {
  from: number;
  to: number;
  color: Color;
}

export interface Color {
  background: string;
  text: string;
}

@Injectable({
  providedIn: 'root'
})
export class BmiService {

  private bmiScales: BmiScale[] = [
    { from: 0, to: 16, color: { background: 'black', text: 'white' } },
    { from: 16, to: 25, color: { background: 'green', text: 'black' } },
    { from: 25, to: 40, color: { background: 'orange', text: 'black' } },
    { from: 40, to: 999, color: { background: 'red', text: 'black' } },
  ];

  constructor() { }

  public getBMI(weight: number, height: number): number | undefined {
    if (!weight || !height) {
      return undefined;
    }

    return Math.floor(weight / Math.pow(height / 100, 2));
  }

  public getColor(bmi: number | undefined): Color {
    if (bmi >= 0) {
      return this.bmiScales.find(threshold => bmi >= threshold.from && bmi < threshold.to).color;
    }

    return { background: 'grey', text: 'white' };
  }
}
